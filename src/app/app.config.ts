import {
  ApplicationConfig,
  importProvidersFrom,
  provideZoneChangeDetection,
} from '@angular/core';
import {
  InMemoryScrollingFeature,
  InMemoryScrollingOptions,
  provideRouter,
  withComponentInputBinding,
  withInMemoryScrolling,
} from '@angular/router';

import { routes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import {
  HttpClient,
  provideHttpClient,
  withInterceptors,
} from '@angular/common/http';
import { authInterceptor } from './interceptors/auth.interceptor';
import { errorForbiddenInterceptor } from './interceptors/error-forbidden.interceptor';
import { provideToastr } from 'ngx-toastr';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {
  TranslateModule,
  TranslateLoader,
  MissingTranslationHandler,
} from '@ngx-translate/core';
import { StaticTranslationLoader } from './static-translations-loader';
import { MyMissingTranslationHandler } from './my-missing-translation-handler';

// bug https://github.com/angular/angular/issues/24547
const scrollConfig: InMemoryScrollingOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
};

const inMemoryScrollingFeature: InMemoryScrollingFeature =
  withInMemoryScrolling(scrollConfig);

// const httpLoaderFactory: (http: HttpClient) => TranslateHttpLoader = (
//   http: HttpClient
// ) => new TranslateHttpLoader(http, './i18n/', '.json');

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(
      routes,
      inMemoryScrollingFeature,
      withComponentInputBinding()
    ),
    provideAnimationsAsync(),
    provideToastr({
      // timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      disableTimeOut: true,
      closeButton: true,
      enableHtml: true,
      tapToDismiss: false,
    }),
    provideHttpClient(
      withInterceptors([
        authInterceptor,
        errorForbiddenInterceptor,
        // authErrorInterceptor,
        // httpErrorInterceptor,
        // httpErrorRetryInterceptor,
      ])
    ),

    provideZoneChangeDetection({ eventCoalescing: true }),
    importProvidersFrom([
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,

          // Using httpLoaderFactory
          // useFactory: httpLoaderFactory,
          // deps: [HttpClient],

          // or include json in bundle
          useClass: StaticTranslationLoader,
        },

        missingTranslationHandler: {
          provide: MissingTranslationHandler,
          useClass: MyMissingTranslationHandler,
        },
      }),
    ]),
  ],
};
