import { Routes } from '@angular/router';
import { FrontComponent } from './front/front.component';
import { SeriesSerDetailComponent } from './front/series/series-ser-detail/series-ser-detail.component';
import { SeriesSearchComponent } from './front/series/series-search/series-search.component';
import { CategoryComponent } from './front/category/category.component';
import { CategoryListComponent } from './front/category/category-list/category-list.component';
import { CategorySingleComponent } from './front/category/category-single/category-single.component';
import { AuthTokenComponent } from './auth/auth-token/auth-token.component';
import { authGuard } from './guards/auth.guard';
import { LogoutComponent } from './auth/logout/logout.component';
import { BookmarkComponent } from './front/bookmark/bookmark.component';
import { SourceComponent } from './front/source/source.component';
import { SourceListComponent } from './front/source/source-list/source-list.component';
import { SourceDetailComponent } from './front/source/source-detail/source-detail.component';
import { ErrorForbiddenComponent } from './errors/error-forbidden/error-forbidden.component';

export const routes: Routes = [
  {
    path: 'auth-token',
    component: AuthTokenComponent,
  },
  { path: 'logout', component: LogoutComponent },
  {
    path: '',
    component: FrontComponent,
    canActivate: [authGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'series',
      },
      {
        path: 'series',
        pathMatch: 'full',
        component: SeriesSearchComponent,
        title: `Rechercher les Séries Numériques`,
      },
      {
        path: 'series/detail/:nameNoSpace',
        component: SeriesSerDetailComponent,
        title: `Série Numérique`,
      },
      {
        path: 'category',
        component: CategoryComponent,
        children: [
          {
            path: '',
            component: CategoryListComponent,
          },
          {
            path: ':id',
            component: CategorySingleComponent,
          },
        ],
      },
      {
        path: 'bookmark',
        component: BookmarkComponent,
      },
      {
        path: 'source',
        component: SourceComponent,
        children: [
          {
            path: '',
            component: SourceListComponent,
          },
          {
            path: ':id',
            component: SourceDetailComponent,
          },
        ],
      },
      { path: 'error/forbidden', component: ErrorForbiddenComponent },
    ],
  },
  {
    path: 'adm',
    // component: BackComponent,
    loadComponent: () =>
      import('./back/back.component').then((m) => m.BackComponent),
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'series',
      },
      {
        path: 'series',
        // component: BackSeriesComponent,
        loadComponent: () =>
          import('./back/back-series/back-series.component').then(
            (m) => m.BackSeriesComponent
          ),
        children: [
          {
            path: '',
            // component: BackSeriesSearchComponent,
            loadComponent: () =>
              import(
                './back/back-series/back-series-search/back-series-search.component'
              ).then((m) => m.BackSeriesSearchComponent),
          },
          {
            path: 'edit/:id',
            // component: BackSeriesEditComponent,
            loadComponent: () =>
              import(
                './back/back-series/back-series-edit/back-series-edit.component'
              ).then((m) => m.BackSeriesEditComponent),
          },
        ],
      },
    ],
  },
  { path: '**', redirectTo: '/' },
];
