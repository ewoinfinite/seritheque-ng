import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-error-forbidden',
  standalone: true,
  imports: [MatCardModule, CommonModule],
  templateUrl: './error-forbidden.component.html',
  styleUrl: './error-forbidden.component.scss',
})
export class ErrorForbiddenComponent {}
