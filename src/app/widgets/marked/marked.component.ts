import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { UtilsService } from '../../utils/utils.service';
import { MarkedOptions } from 'marked';
import { SafeHtmlPipe } from '../../utils/safe-html.pipe';
import {
  BehaviorSubject,
  Observable,
  audit,
  combineLatest,
  distinctUntilChanged,
  map,
  timer,
} from 'rxjs';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-marked',
  standalone: true,
  imports: [CommonModule, SafeHtmlPipe],
  templateUrl: './marked.component.html',
  styleUrl: './marked.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarkedComponent {
  @Input()
  set text(text: string | null) {
    this._text$.next(text);
  }

  @Input()
  set lang(lang: string | null) {
    this._lang$.next(lang);
  }

  @Input()
  options?: Omit<MarkedOptions, 'async'>;

  @Input()
  safeHtml?: boolean;

  @Input()
  time?: number;

  private readonly _text$ = new BehaviorSubject<string | null>(
    this.text ?? null
  );

  private readonly _lang$ = new BehaviorSubject<string | null>(
    this.lang ?? null
  );

  readonly data$: Observable<{
    lang: string | null;
    text: string | null;
  }> = combineLatest([this._lang$, this._text$]).pipe(
    audit(() => timer(this.time ?? 0)),
    distinctUntilChanged(),
    map(([lang, text]) => ({ lang, text }))
  );

  readonly text$ = this.data$.pipe(map((data) => data.text));

  readonly lang$ = this.data$.pipe(map((data) => data.lang));

  private readonly _defaultOptions = <Omit<MarkedOptions, 'async'>>{
    silent: true,
    gfm: true,
    breaks: true,
  };

  constructor(private readonly utils: UtilsService) {}

  md2html(md: string) {
    return this.utils.fixHtml(
      this.utils.md2html(this.utils.txtLocal(md, this.lang), {
        ...this._defaultOptions,
        ...this.options,
      })
    );
  }
}
