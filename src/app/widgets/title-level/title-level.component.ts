import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title-level',
  standalone: true,
  imports: [],
  templateUrl: './title-level.component.html',
  styleUrl: './title-level.component.scss',
})
export class TitleLevelComponent {
  @Input()
  level?: number;

  @Input()
  text?: string;
}
