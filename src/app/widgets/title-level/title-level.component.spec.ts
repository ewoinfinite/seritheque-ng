import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleLevelComponent } from './title-level.component';

describe('TitleLevelComponent', () => {
  let component: TitleLevelComponent;
  let fixture: ComponentFixture<TitleLevelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TitleLevelComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TitleLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
