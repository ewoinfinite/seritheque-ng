import { TranslateLoader, TranslationObject } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

import * as translationsEN from '../../public/i18n/en.json';
import * as translationsES from '../../public/i18n/es.json';
import * as translationsFR from '../../public/i18n/fr.json';
import * as translationsIT from '../../public/i18n/it.json';
import * as translationsPT from '../../public/i18n/pt.json';
import * as translationsDE from '../../public/i18n/de.json';

// interface Translation {
//     [key: string]: string | Translation;
// }

const TRANSLATIONS: TranslationObject = {
  en: translationsEN,
  es: translationsES,
  fr: translationsFR,
  it: translationsIT,
  pt: translationsPT,
  de: translationsDE,
};

export class StaticTranslationLoader implements TranslateLoader {
  public getTranslation(lang: string): Observable<TranslationObject> {
    const translation = TRANSLATIONS[lang];
    if (translation) {
      return of(translation);
    } else {
      console.error(`Unknown language: ${lang}`);
      return of({});
    }
  }
}
