import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthUserResponse, AuthService } from '../auth.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-logout',
  standalone: true,
  imports: [],
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.scss',
})
export class LogoutComponent implements OnInit {
  isAuthenticated$: Observable<boolean>;
  user$: Observable<AuthUserResponse | null>;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {
    this.isAuthenticated$ = this.authService.isAuthenticated$;
    this.user$ = this.authService.user$;
  }

  ngOnInit(): void {
    this.authService.logout();

    window.location.href = environment.logoutUrl;
  }
}
