import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthTokenService {
  get(): string | null {
    return localStorage.getItem('token');
  }

  set(token: string): void {
    localStorage.setItem('token', token);
  }

  remove(): void {
    localStorage.removeItem('token');
  }
}
