import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, distinctUntilChanged, first } from 'rxjs';
import { AuthTokenService } from '../auth-token.service';

@Component({
  selector: 'app-auth-token',
  standalone: true,
  imports: [],
  templateUrl: './auth-token.component.html',
  styleUrl: './auth-token.component.scss',
})
export class AuthTokenComponent implements OnInit, OnDestroy {
  private readonly subscription = new Subscription();

  errorMessage: string | undefined;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly authTokenService: AuthTokenService
  ) {}

  ngOnInit(): void {
    this.subscription.add(
      this.route.queryParams.pipe(first()).subscribe((params) => {
        const { auth_token } = params;

        if (!auth_token) {
          this.errorMessage = 'Missing token';
          return;
        }

        this.authTokenService.set(auth_token);

        this.router.navigate(['/'], {
          relativeTo: this.route,
          replaceUrl: true,
          queryParams: {},
          queryParamsHandling: '',
        });
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
