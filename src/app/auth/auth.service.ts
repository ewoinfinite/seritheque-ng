import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  Subject,
  catchError,
  combineLatest,
  debounceTime,
  distinctUntilChanged,
  first,
  firstValueFrom,
  map,
  of,
  shareReplay,
  startWith,
  switchMap,
  take,
  tap,
  timer,
} from 'rxjs';
import { environment } from '../../environments/environment';
import { apiAuthUser } from '../constants';
import { filterIsNot } from '../utils/rxjs-filter-is-not.pipe';
import { AuthTokenService } from './auth-token.service';
import { Lang } from '../types/lang.type';

export interface AuthLoginResponse {
  token: string;
}

export interface AuthLogoutResponse {
  success: boolean;
  message: string;
}

export interface AuthLoginBody {
  username: string;
  password: string;
}

export interface AuthToken {
  id: string;
  createdAt: Date;
  expiresAt: Date | null;
}

// export interface AuthUserRole {
//   id: string;
//   name: string;
// }

export interface AuthUserResponse {
  id: string;
  provider: string | null;
  providerId: string | null;
  authToken: AuthToken;
  name: string;
  email: string;
  langId: string | null;
  lang: Lang | null;
  role: string;
  isSuperAdmin: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // aka having the token in localStorage
  readonly isLoggedIn$ = new BehaviorSubject(this.isLoggedIn());

  /**
   * undefined means we are checking from api (not authenticated yet)
   * null means we are not authenticated against the api
   */

  /**
   * dummy Subject to trigger an update of _user$
   */
  private readonly _userUpdate$ = new Subject<void>();

  private readonly _user$ = combineLatest([
    this.isLoggedIn$.pipe(distinctUntilChanged()),
    this._userUpdate$.pipe(startWith(undefined)),
  ]).pipe(
    map(([isLoggedIn]) => isLoggedIn),
    switchMap((isLoggedIn) => {
      if (isLoggedIn) {
        return this.getUser().pipe(
          startWith(undefined), // signal we are updating
          take(2) // completes after we get a user
        );
      }

      return of(null);
    }),
    shareReplay(1) // keep data until isLoggedIn$ changes
  );

  readonly user$: Observable<AuthUserResponse | null> = this._user$.pipe(
    filterIsNot(undefined)
  );

  readonly isAuthenticated$: Observable<boolean> = this.user$.pipe(
    map((user) => !!user),
    // make isAuthenticated$ simple to use
    catchError((error) => {
      console.error(error);
      return of(false);
    })
  );

  constructor(
    private readonly http: HttpClient,
    private readonly tokenService: AuthTokenService
  ) {}

  /**
   *
   * @param username
   * @param password
   * @returns {Observable<AuthLoginResponse>}
   */

  login$(username: string, password: string): Observable<void> {
    const body: AuthLoginBody = {
      username,
      password,
    };

    return this.http
      .post<AuthLoginResponse>(`${environment.apiBase}/auth/login`, body)
      .pipe(
        tap((response) => this.setToken(response)),
        map(() => undefined) // silence is gold
      );
  }

  /**
   *
   * @returns {Observable<AuthLogoutResponse>}
   */

  logout$(): Observable<AuthLogoutResponse> {
    return combineLatest([
      this.http
        .post<AuthLogoutResponse>(`${environment.apiBase}/auth/logout`, {})
        .pipe(
          first(),
          tap({
            error: () =>
              console.error(
                'Failed to logout from api (local token was deleted)'
              ),
          })
        ),
      timer(0).pipe(tap(() => this.removeToken())),
    ]).pipe(map(([response]) => response));
  }

  isLoggedIn(): boolean {
    return !!this.tokenService.get();
  }

  userUpdate() {
    this._userUpdate$.next(undefined);
  }

  private getUser(): Observable<AuthUserResponse> {
    return this.http.get<AuthUserResponse>(apiAuthUser);
  }

  private setToken(authLoginResponse: AuthLoginResponse) {
    this.tokenService.set(authLoginResponse.token);
    this.isLoggedIn$.next(this.isLoggedIn());
  }

  private removeToken() {
    this.tokenService.remove();
    this.isLoggedIn$.next(this.isLoggedIn());
  }

  /**
   * Some handy async utilities
   */

  async login(username: string, password: string): Promise<void> {
    return firstValueFrom(this.login$(username, password));
  }

  async logout(): Promise<AuthLogoutResponse> {
    return firstValueFrom(this.logout$());
  }

  async user() {
    return firstValueFrom(this.user$);
  }

  async isAuthenticated() {
    return firstValueFrom(this.isAuthenticated$);
  }
}
