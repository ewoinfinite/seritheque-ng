import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  Observable,
  delay,
  first,
  shareReplay,
  startWith,
  switchMap,
  take,
  tap,
} from 'rxjs';
import { environment } from '../environments/environment';
import { PaginatedResult } from './types/paginated-result.type';
import { SerDetail } from './types/ser-detail.type';
import { CategoryWithContents } from './types/category.type';
import { SimpleSerDetail } from './types/ser-detail-simple.type';
import { SimpleCategory } from './types/simple-category.type';
import { SimpleCategoryWithCount } from './types/simple-category-with-count.type';
import { SerFindManyItem } from './types/ser-find-many-item.type';
import { SourceWithContents } from './types/source.type';
import { SimpleSource } from './types/simple-source.type';
import { pickBy } from 'lodash-es';
import { Bookmark } from './types/bookmark.type';
import { TranslateService } from '@ngx-translate/core';

export type SerSearch = {
  q?: string;
  langId?: string;
  titleOnly?: boolean;
  sourceIds?: number[];
  page?: number;
};

@Injectable({
  providedIn: 'root',
})
export class AppService {
  private serSearchHistory: SerSearch[] = [];

  constructor(
    private readonly http: HttpClient,
    private readonly translate: TranslateService
  ) {
    this.translate.onLangChange
      .pipe(tap(() => (this._bookmarkFindManyCache$ = undefined)))
      .subscribe();
  }

  serSearch(args?: SerSearch): Observable<PaginatedResult<SerFindManyItem>> {
    // console.log(args);

    const args2: {
      q?: string;
      langId?: string;
      titleOnly?: number;
      'sourceIds[]'?: number[];
      page?: number;
    } = {
      q: args?.q,
      langId: args?.langId || this.translate.currentLang,
      titleOnly: args?.titleOnly ? 1 : undefined,
      'sourceIds[]': args?.sourceIds,
      page: args?.page,
    };

    const args3 = pickBy(args2);

    return this.http.get<PaginatedResult<SerFindManyItem>>(
      `${environment.apiBase}/ser`,
      {
        params: new HttpParams({ fromObject: { ...args3 } }),
      }
    );
  }

  serFindById(id: number): Observable<SerDetail> {
    return this.http.get<SerDetail>(`${environment.apiBase}/ser/by-id/${id}`);
  }

  serFindByIdSimple(id: number, langId: string): Observable<SimpleSerDetail> {
    return this.http.get<SimpleSerDetail>(
      `${environment.apiBase}/ser/by-id-simple/${id}/${langId}`
    );
  }

  serFindByNameSimple(
    nameNoSpace: string
    // langId: string = this.translate.currentLang
  ): Observable<SimpleSerDetail[]> {
    // console.log(this.translate.currentLang);
    return this.translate.onLangChange.pipe(
      startWith(this.translate.currentLang),
      // tap(console.log),

      switchMap((lang) =>
        this.http.get<SimpleSerDetail[]>(
          `${environment.apiBase}/ser/by-name-simple/${nameNoSpace}/${this.translate.currentLang}`
        )
      )
    );
  }

  categoryFindMany(args?: {
    langId: string;
  }): Observable<CategoryWithContents[]> {
    return this.http
      .get<CategoryWithContents[]>(`${environment.apiBase}/category`, {
        params: new HttpParams({ fromObject: args }),
      })
      .pipe(first(), shareReplay(1));
  }

  simpleCategoryFindMany(
    langId: string,
    args?: any
  ): Observable<SimpleCategoryWithCount[]> {
    return this.http
      .get<SimpleCategoryWithCount[]>(
        `${environment.apiBase}/category/simple/${langId}`,
        {
          params: new HttpParams({ fromObject: args }),
        }
      )
      .pipe(first(), shareReplay(1));
  }

  sourceFindMany(): Observable<SourceWithContents[]> {
    return this.http
      .get<SourceWithContents[]>(`${environment.apiBase}/source/?langId=fr`)
      .pipe(shareReplay(1), first());
  }

  simpleSourceFindMany(): Observable<SimpleSource[]> {
    return this.translate.onLangChange.pipe(
      startWith(this.translate.currentLang),
      // tap(console.log),
      switchMap((lang) =>
        this.http.get<SimpleSource[]>(
          `${environment.apiBase}/source/simple?langId=${this.translate.currentLang}`
        )
      )
    );
    // return this.http
    //   .get<SimpleSource[]>(`${environment.apiBase}/source/simple?langId=fr`)
    //   .pipe(shareReplay(1), first());
  }

  private _bookmarkFindManyCache$?: Observable<Bookmark[]>;

  bookmarkFindMany(): Observable<Bookmark[]> {
    // if (!this._bookmarkFindManyCache$) {
    //   this._bookmarkFindManyCache$ = this.http
    //     .get<Bookmark[]>(`${environment.apiBase}/bookmark/?langId=fr`)
    //     .pipe(take(1), shareReplay(1));
    // }

    if (!this._bookmarkFindManyCache$) {
      this._bookmarkFindManyCache$ = this.translate.onLangChange.pipe(
        startWith(this.translate.currentLang),
        switchMap(() =>
          this.http.get<Bookmark[]>(
            `${environment.apiBase}/bookmark/?langId=${this.translate.currentLang}`
          )
        ),
        // take(1),
        shareReplay(1)
      );
    }

    return this._bookmarkFindManyCache$;
  }

  bookmarkCreate(label: string): Observable<Bookmark> {
    return this.http
      .post<Bookmark>(`${environment.apiBase}/bookmark/`, {
        label,
      })
      .pipe(tap(() => (this._bookmarkFindManyCache$ = undefined)));
  }

  bookmarkAddSer(serId: number, label: string = 'default'): Observable<any> {
    return this.http
      .post(`${environment.apiBase}/bookmark/${label}/${serId}`, {})
      .pipe(tap(() => (this._bookmarkFindManyCache$ = undefined)));
  }

  bookmarkDelSer(serId: number, label: string = 'default'): Observable<any> {
    return this.http
      .delete(`${environment.apiBase}/bookmark/${label}/${serId}`)
      .pipe(tap(() => (this._bookmarkFindManyCache$ = undefined)));
  }

  serSearchHistoryPush(search: SerSearch) {
    this.serSearchHistory.push(search);
  }

  serSearchHistoryGet(): SerSearch[] {
    return this.serSearchHistory;
  }

  serSearchHistoryGetLast(): SerSearch | null {
    return this.serSearchHistory.length
      ? this.serSearchHistory[this.serSearchHistory.length - 1]
      : null;
  }
}
