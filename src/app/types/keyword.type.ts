export type Keyword = {
  id: number;
  langId: string;
  word: string;
  noAccentWord?: string;
  createdAt?: Date;
  updatedAt?: Date;
};
