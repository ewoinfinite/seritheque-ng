export type TagContent = {
  tagId: number;
  langId: string;
  name: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
};
