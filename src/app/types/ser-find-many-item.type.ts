export type SerFindManyItemContent = {
  id: number;
  title: string;
  excerpt: string;
};

export type SerFindManyItem = {
  id: number;
  name: string;
  nameNoSpace: string;
  contents: SerFindManyItemContent[];
};
