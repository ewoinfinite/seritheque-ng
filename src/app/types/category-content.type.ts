export type CategoryContent = {
  categoryId: number;
  langId: string;
  name: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
};
