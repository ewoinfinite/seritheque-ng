export type Ser = {
  id: number;
  name: string;
  nameNoSpace: string;
  createdAt: Date;
  updatedAt: Date;
};
