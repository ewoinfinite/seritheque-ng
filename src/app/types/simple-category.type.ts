export type SimpleCategory = {
  id: number;
  parentId: number | null;
  order: number | null;
  name: string;
};
