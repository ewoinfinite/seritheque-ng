export type SimpleSource = {
  id: number;
  title: string;
  isbn: string | null;
  description: string | null;
};
