import { CategoryContent } from './category-content.type';

export type Category = {
  id: number;
  parentId: number;
  order: number;
  tmpName: string;
  createdAt: Date;
  updatedAt: Date;
};

export type CategoryWithContents = Category & { contents: CategoryContent[] };
