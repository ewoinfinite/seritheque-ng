import { SourceContent } from './source-content.type';

export type Source = {
  id: number;
  tmpName: string | null;
  createdAt: Date;
  updatedAt: Date;
};

export type SourceWithContents = Source & {
  contents: SourceContent[];
};
