export type SourceContent = {
  sourceId: number;
  langId: string;
  title: string;
  description: string | null;
  isbn: string | null;
  createdAt: Date;
  updatedAt: Date;
};
