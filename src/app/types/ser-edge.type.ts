export type SerEdge = {
  fromId: number;
  toId: number;
  order: number;
};
