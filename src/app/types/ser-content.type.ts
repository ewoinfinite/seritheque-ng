export type SerContent = {
  id: number;
  serId: number;
  langId: string;
  title: string;
  content: string;
  excerpt: string;
  noAccentTitle: string;
  createdAt: Date;
  updatedAt: Date;
};
