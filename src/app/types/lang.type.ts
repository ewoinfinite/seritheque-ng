export type Lang = {
  id: string;
  name: string;
  localName: string;
};
