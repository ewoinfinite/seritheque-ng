export type Tag = {
  id: number;
  tmpName: string;
  createdAt: Date;
  updatedAt: Date;
};
