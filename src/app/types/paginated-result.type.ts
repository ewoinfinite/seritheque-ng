export interface PaginatedMeta {
  total: number;
  lastPage: number;
  currentPage: number;
  perPage: number;
  prev: number | null;
  next: number | null;
}

export interface PaginatedResult<T = any> {
  data: T[];
  meta: PaginatedMeta;
}
