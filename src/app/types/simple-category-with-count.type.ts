import { SimpleCategory } from './simple-category.type';

export type SimpleCategoryWithCount = SimpleCategory & {
  count: number;
};
