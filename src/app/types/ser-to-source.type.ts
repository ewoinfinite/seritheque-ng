export type SerToSource = {
  serId: string;
  sourceId: number;
  reference: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
};
