import { Category } from './category.type';
import { CategoryContent } from './category-content.type';
import { SerContent } from './ser-content.type';
import { Ser } from './ser.type';
import { TagContent } from './tag-content.type';
import { Tag } from './tag.type';
import { Keyword } from './keyword.type';
import { SerEdge } from './ser-edge.type';
import { SerToSource } from './ser-to-source.type';
import { Source } from './source.type';
import { SourceContent } from './source-content.type';

export type SerDetail = Ser & {
  contents: SerContent[];
  categories: (Category & { contents: CategoryContent[] })[];
  tags: (Tag & { contents: TagContent[] })[];
  keywords: Keyword[];
  edges: (SerEdge & { to: Ser & { contents: SerContent[] } })[];
  revEdges: (SerEdge & { from: Ser & { contents: SerContent[] } })[];
  serToSources: (SerToSource & {
    source: Source & { contents: SourceContent[] };
  })[];
};
