import { SimpleCategory } from './simple-category.type';
import { Keyword } from './keyword.type';
import { SerContent } from './ser-content.type';
import { SerEdge } from './ser-edge.type';
import { SerToSource } from './ser-to-source.type';
import { Ser } from './ser.type';
import { SourceContent } from './source-content.type';
import { Source } from './source.type';
import { TagContent } from './tag-content.type';
import { Tag } from './tag.type';

export type SimpleSerDetail = Ser & {
  contents: SerContent[];
  simpleCategories: SimpleCategory[];
  tags: (Tag & { contents: TagContent[] })[];
  keywords: Keyword[];
  edges: (SerEdge & { to: Ser & { contents: SerContent[] } })[];
  revEdges: (SerEdge & { from: Ser & { contents: SerContent[] } })[];
  source: Source & { contents: SourceContent[] };
};
