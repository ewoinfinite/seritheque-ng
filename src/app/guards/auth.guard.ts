import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { inject } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { environment } from '../../environments/environment';

export const authGuard: CanActivateFn = async (_route, state) => {
  const authService = inject(AuthService);
  const router = inject(Router);

  // not only we have a token in local storage but we also got an authenticated user from api
  const isAuthenticated = await firstValueFrom(authService.isAuthenticated$);

  if (isAuthenticated === true) {
    return true;
  }

  // redirect to login keeping current url into window.history.state.redirect
  // router.navigateByUrl('/login', {
  //   state: { redirect: state.url },
  // });

  window.location.href = `${environment.apiBase}${environment.oauthEndpoint}`;

  return false;
};
