import { Component, Inject, PLATFORM_ID } from '@angular/core';
import { BackNavComponent } from './back-nav/back-nav.component';
import { RouterModule } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-back',
  standalone: true,
  imports: [BackNavComponent, RouterModule],
  templateUrl: './back.component.html',
  styleUrl: './back.component.scss',
})
export class BackComponent {
  public isBrowser = isPlatformBrowser(this.platformId);

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {}
}
