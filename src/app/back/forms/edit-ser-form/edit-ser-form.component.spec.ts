import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSerFormComponent } from './edit-ser-form.component';

describe('EditSerFormComponent', () => {
  let component: EditSerFormComponent;
  let fixture: ComponentFixture<EditSerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EditSerFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EditSerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
