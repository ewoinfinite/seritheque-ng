import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  FormsModule,
  NonNullableFormBuilder,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MarkedComponent } from '../../../widgets/marked/marked.component';
import { SafeHtmlPipe } from '../../../utils/safe-html.pipe';
import { UtilsService } from '../../../utils/utils.service';
import { AppService } from '../../../app.service';

import { TreeSelect, TreeSelectModule } from 'primeng/treeselect';
import { ChipModule } from 'primeng/chip';
import {
  Observable,
  Subscription,
  combineLatest,
  distinctUntilChanged,
  firstValueFrom,
  map,
  startWith,
  tap,
} from 'rxjs';
import { isDefined } from '../../../utils/filter-is-defined';
import * as _ from 'lodash-es';
import { SimpleSerDetail } from '../../../types/ser-detail-simple.type';
import { TreeNode } from 'primeng/api';
import { SimpleCategory } from '../../../types/simple-category.type';
import { TreeNodeSelectEvent } from 'primeng/tree';
import { Button, ButtonModule } from 'primeng/button';
import { TimesCircleIcon } from 'primeng/icons/timescircle';
import { Keyword } from '../../../types/keyword.type';
import { SerToSource } from '../../../types/ser-to-source.type';
import { SourceWithContents } from '../../../types/source.type';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';

export type FormGroupMap<T> = FormGroup<{
  [key in keyof T]: FormControl<T[key] | undefined | null>;
}>;

export type NotNullableFormGroupMap<T> = FormGroup<{
  [key in keyof T]: FormControl<T[key]>;
}>;

@Component({
  selector: 'app-edit-ser-form',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatCardModule,
    MarkedComponent,
    SafeHtmlPipe,
    MarkedComponent,
    // MatSelectModule,
    // MatFormFieldModule,
    TreeSelectModule,
    ChipModule,
    ButtonModule,
    FormsModule,
    TimesCircleIcon,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
  ],
  templateUrl: './edit-ser-form.component.html',
  styleUrl: './edit-ser-form.component.scss',
  encapsulation: ViewEncapsulation.None,
  // changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: true,
})
export class EditSerFormComponent implements OnChanges, OnDestroy {
  subscription = new Subscription();

  @Input()
  input?: SimpleSerDetail;

  form!: typeof this.formGroup;

  readonly categories$ = this.service.simpleCategoryFindMany('fr');

  categoryTreeNodes$: Observable<TreeNode<SimpleCategory>[]>;

  @ViewChild('addSimpleCategoryTreeSelect')
  addSimpleCategoryTreeSelect!: TreeSelect;

  addSimpleCategorySelectedItem: any;

  @ViewChild('addSimpleCategoryButton', { read: ElementRef, static: false })
  addSimpleCategoryButton!: ElementRef<HTMLButtonElement>;

  sources$: Observable<SourceWithContents[]>;

  constructor(
    private readonly fb: NonNullableFormBuilder,
    private readonly utils: UtilsService,
    private readonly service: AppService
  ) {
    this.form = this.formGroup;

    this.categoryTreeNodes$ = combineLatest([
      this.categories$,
      this.form.controls.simpleCategories.valueChanges.pipe(
        startWith(this.form.controls.simpleCategories.value),
        map((controls) =>
          controls.map((control) => control.id).filter(isDefined)
        )
      ),
    ]).pipe(
      map(([categories, formCategories]) =>
        this.utils.flatToPrimeTreeNodes(
          categories,
          (item) => item.name ?? String(item.id),
          formCategories
        )
      )
    );

    this.subscription.add(
      this.form.valueChanges
        .pipe(
          distinctUntilChanged((prev, curr) => _.isEqual(prev, curr))
          // tap(console.log)
        )
        .subscribe()
    );

    this.sources$ = this.service.sourceFindMany();
  }

  get formGroup() {
    return this.fb.group({
      id: new FormControl<number>(0, [Validators.required]),
      name: new FormControl<string>('', [Validators.required]),
      contents: this.fb.array<typeof this.contentGroup>([]),
      simpleCategories: this.fb.array<
        ReturnType<typeof this.simpleCategoryGroup>
      >([]),
      keywords: this.fb.array<ReturnType<typeof this.keywordGroup>>([]),
      sourceId: new FormControl<number | null>(null),
      sourceReference: new FormControl<string | null>(null),
    });
  }

  get contentGroup() {
    return this.fb.group({
      id: [0, [Validators.required]],
      serId: [0],
      langId: ['', [Validators.required]],
      title: ['', [Validators.required]],
      content: [''],
      // createdAt: [],
      // updatedAt: [],
    });
  }

  get formContents() {
    return <FormArray<typeof this.contentGroup>>this.form.get('contents');
  }

  simpleCategoryGroup(data?: SimpleCategory) {
    return this.fb.group({
      id: new FormControl<number>(data ? data.id : 0, [Validators.required]),

      // this is just for convenance, at api we only send id
      parentId: new FormControl<number | null>(data ? data.parentId : null),
      order: new FormControl<number | null>(data ? data.order : null),
      name: new FormControl<string>(data ? data.name : '', [
        Validators.required,
      ]),
    });
  }

  get formSimpleCategories() {
    return <FormArray<ReturnType<typeof this.simpleCategoryGroup>>>(
      this.form.get('simpleCategories')
    );
  }

  keywordGroup(data?: Keyword) {
    return this.fb.group({
      id: new FormControl<number | null>(data ? data.id : null),
      langId: new FormControl<string>(data ? data.langId : 'fr', [
        Validators.required,
      ]),
      word: new FormControl<string>(data ? data.word : '', [
        Validators.required,
        Validators.minLength(3),
      ]),
    });
  }

  get formKeywords() {
    return <FormArray<ReturnType<typeof this.keywordGroup>>>(
      this.form.get('keywords')
    );
  }

  serToSourceGroup(data: SerToSource) {
    return this.fb.group({
      sourceId: new FormControl<number>(data.sourceId),
      reference: new FormControl<string | null>(data.reference ?? null),
      description: new FormControl<string | null>(data.description ?? null),
    });
  }

  updateForm(input: Partial<SimpleSerDetail>) {
    if (input.contents) {
      this.formContents.clear({ emitEvent: false });
      input.contents.forEach((content) => {
        this.formContents.push(this.contentGroup, { emitEvent: false });
      });
    }

    if (input.simpleCategories) {
      this.formSimpleCategories.clear({ emitEvent: false });
      input.simpleCategories.forEach((category) => {
        this.formSimpleCategories.push(this.simpleCategoryGroup(category), {
          emitEvent: false,
        });
      });
    }

    if (input.keywords) {
      this.formKeywords.clear({ emitEvent: false });
      input.keywords.forEach((keyword) => {
        this.formKeywords.push(this.keywordGroup(keyword), {
          emitEvent: false,
        });
      });
    }

    this.form.patchValue(input);
    this.form.markAsPristine();
  }

  addSimpleCategoryOnSelect(event: TreeNodeSelectEvent) {
    event.originalEvent.stopPropagation();

    this.addSimpleCategoryTreeSelect.hide();
    this.addSimpleCategoryButton.nativeElement.focus();
  }

  addSimpleCategory(e?: MouseEvent | KeyboardEvent) {
    if (
      e instanceof MouseEvent ||
      (e instanceof KeyboardEvent && [' ', 'Enter'].includes(e.key))
    ) {
      e.stopPropagation();
      e.preventDefault();
    } else if (e) {
      // we have an event and it's not one we should care about
      return;
    }

    this.formSimpleCategories.push(
      this.simpleCategoryGroup(this.addSimpleCategorySelectedItem.data)
    );

    this.addSimpleCategorySelectedItem = undefined;
  }

  removeSimpleCategory(i: number, e?: MouseEvent | KeyboardEvent) {
    if (
      e instanceof MouseEvent ||
      (e instanceof KeyboardEvent && [' ', 'Enter'].includes(e.key))
    ) {
      e.stopPropagation();
      e.preventDefault();
    } else if (e) {
      // we have an event and it's not one we should care about
      return;
    }

    if (this.removeSimpleCategoryConfirm()) {
      this.formSimpleCategories.removeAt(i);
    }

    return;
  }

  removeSimpleCategoryConfirm(): boolean {
    return window.confirm(
      `Êtes-vous certains de vouloir supprimer l'association à la catégorie ?`
    );
  }

  // using async pipe in table on Promise seems to get lost in loop
  // async getCategoryById(id: number): Promise<SimpleCategory | null> {
  //   const categories = await firstValueFrom(this.categories$);

  //   console.log(categories);
  //   const result = categories.find((category) => category.id === id) ?? null;
  //   // if (!isDefined(result))
  //   //   throw new Error(`No matching category for id:${id}`);

  //   return result;
  // }

  getCategoryById$(id: number): Observable<SimpleCategory | null> {
    return this.categories$.pipe(
      map(
        (categories) =>
          categories.find((category) => category.id === id) ?? null
      )
    );
  }

  removeKeyword(i: number, e: MouseEvent | KeyboardEvent) {
    if (
      e instanceof MouseEvent ||
      (e instanceof KeyboardEvent && [' ', 'Enter'].includes(e.key))
    ) {
      e.stopPropagation();
      e.preventDefault();
    } else if (e) {
      // we have an event and it's not one we should care about
      return;
    }

    if (
      this.formKeywords.at(i).controls.word.value?.trim() == '' ||
      this.removeKeywordConfirm()
    ) {
      this.formKeywords.removeAt(i);
    }

    return;
  }

  removeKeywordConfirm(): boolean {
    return window.confirm(
      `Êtes-vous certains de vouloir supprimer ce mot clé ?`
    );
  }

  addKeyword(e: MouseEvent | KeyboardEvent) {
    if (
      e instanceof MouseEvent ||
      (e instanceof KeyboardEvent && [' ', 'Enter'].includes(e.key))
    ) {
      e.stopPropagation();
      e.preventDefault();
    } else if (e) {
      // we have an event and it's not one we should care about
      return;
    }

    this.formKeywords.push(this.keywordGroup());
  }

  ngOnChanges(changes: SimpleChanges): void {
    const input: SimpleSerDetail = changes['input']?.currentValue;
    if (input) {
      this.updateForm(input);
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  md2html = this.utils.md2html;

  txtLocal = this.utils.txtLocal;
}
