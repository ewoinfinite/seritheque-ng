import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackSeriesEditComponent } from './back-series-edit.component';

describe('BackSeriesEditComponent', () => {
  let component: BackSeriesEditComponent;
  let fixture: ComponentFixture<BackSeriesEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BackSeriesEditComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BackSeriesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
