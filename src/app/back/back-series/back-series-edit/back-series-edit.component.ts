import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { SerDetail } from '../../../types/ser-detail.type';
import { EditSerFormComponent } from '../../forms/edit-ser-form/edit-ser-form.component';
import { AppService } from '../../../app.service';
import { SimpleSerDetail } from '../../../types/ser-detail-simple.type';

@Component({
  selector: 'app-back-series-edit',
  standalone: true,
  imports: [CommonModule, EditSerFormComponent],
  templateUrl: './back-series-edit.component.html',
  styleUrl: './back-series-edit.component.scss',
})
export class BackSeriesEditComponent {
  readonly subscription = new Subscription();

  ser?: SimpleSerDetail;

  @Input()
  set id(id: string) {
    this.subscription.add(
      this.service.serFindByIdSimple(+id, 'fr').subscribe((ser) => {
        this.ser = ser;
      })
    );
  }

  constructor(private readonly service: AppService) {}
}
