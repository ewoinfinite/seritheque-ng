import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackSeriesComponent } from './back-series.component';

describe('BackSeriesComponent', () => {
  let component: BackSeriesComponent;
  let fixture: ComponentFixture<BackSeriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BackSeriesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BackSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
