import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackSeriesSearchComponent } from './back-series-search.component';

describe('BackSeriesSearchComponent', () => {
  let component: BackSeriesSearchComponent;
  let fixture: ComponentFixture<BackSeriesSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BackSeriesSearchComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BackSeriesSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
