import { Component, OnDestroy } from '@angular/core';
import { BackSeriesSearchFormComponent } from './back-series-search-form/back-series-search-form.component';
import { BackSeriesSearchListComponent } from './back-series-search-list/back-series-search-list.component';
import { IInfiniteScrollEvent } from 'ngx-infinite-scroll';
import {
  Subscription,
  BehaviorSubject,
  Observable,
  combineLatest,
  map,
  tap,
  distinctUntilChanged,
  debounceTime,
  mergeMap,
  scan,
} from 'rxjs';
import { SeriesSearchForm } from '../../../front/series/series-search/series-search-form/series-search-form.component';
import {
  PaginatedResult,
  PaginatedMeta,
} from '../../../types/paginated-result.type';
import { filterIsDefined } from '../../../utils/rxjs-filter-is-defined.pipe';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';
import { AppService } from '../../../app.service';
import { SerFindManyItem } from '../../../types/ser-find-many-item.type';
import { pickBy } from 'lodash-es';

@Component({
  selector: 'app-back-series-search',
  standalone: true,
  imports: [
    CommonModule,
    BackSeriesSearchFormComponent,
    BackSeriesSearchListComponent,
    MatCardModule,
  ],
  templateUrl: './back-series-search.component.html',
  styleUrl: './back-series-search.component.scss',
})
export class BackSeriesSearchComponent implements OnDestroy {
  subscription = new Subscription();

  search$ = new BehaviorSubject<SeriesSearchForm | null>(null);

  page$ = new BehaviorSubject<number>(1);

  loading$ = new BehaviorSubject<boolean>(false);
  request$: Observable<PaginatedResult<SerFindManyItem>>;
  data$: Observable<SerFindManyItem[]>;
  meta: PaginatedMeta | null = null;

  scrollLimit = 4;

  constructor(private readonly service: AppService) {
    this.request$ = combineLatest([
      this.search$.pipe(
        filterIsDefined(),
        map((search) => {
          search.q = search.q && search.q.trim();
          return search;
        }),
        // filter((search) => !!search?.q),
        tap(() => {
          this.page$.next(1);
          this.meta = null;
        })
      ),
      this.page$.pipe(distinctUntilChanged()),
    ]).pipe(
      debounceTime(0),
      // filter(([search]) => !!search?.q), // filter out empty search
      tap(() => this.loading$.next(true)),
      mergeMap(([search, page]) =>
        this.service.serSearch({ ...pickBy(search), page })
      ),
      tap((response) => {
        this.meta = response.meta;
      }),
      tap(() => this.loading$.next(false))
    );

    this.data$ = this.request$.pipe(
      // accumulate data for infinityScroll
      scan((acc, val) => {
        if (val.meta?.prev) {
          return acc.concat(val.data);
        }
        // if this is first page then it's a new search, reset data
        return val.data;
      }, <SerFindManyItem[]>[])
    );
  }

  search(form: any) {
    this.search$.next(form);
  }

  loadMore() {
    if (this.meta?.next && this.meta.currentPage === this.page$.value) {
      this.page$.next(this.meta.next);
    }
  }

  onScroll(event?: IInfiniteScrollEvent) {
    if (this.meta?.currentPage && this.meta?.currentPage < this.scrollLimit) {
      this.loadMore();
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
