import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackSeriesSearchFormComponent } from './back-series-search-form.component';

describe('BackSeriesSearchFormComponent', () => {
  let component: BackSeriesSearchFormComponent;
  let fixture: ComponentFixture<BackSeriesSearchFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BackSeriesSearchFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BackSeriesSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
