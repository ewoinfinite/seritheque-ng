import {
  Component,
  EventEmitter,
  Output,
  ViewChild,
  viewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatFormFieldModule,
} from '@angular/material/form-field';
import { MatIcon } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { Router, ActivatedRoute } from '@angular/router';
import { distinctUntilChanged } from 'rxjs';

export interface BackSeriesSearchForm {
  q: string;
  lang: string;
}

@Component({
  selector: 'app-back-series-search-form',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIcon,
    MatButtonModule,
  ],
  templateUrl: './back-series-search-form.component.html',
  styleUrl: './back-series-search-form.component.scss',
  providers: [
    // sets default value for all mat-form-fields
    // aka <mat-form-field subscriptSizing="dynamic">
    // credit https://stackoverflow.com/questions/53684763/how-to-remove-space-bottom-mat-form-field
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        subscriptSizing: 'dynamic',
      },
    },
  ],
})
export class BackSeriesSearchFormComponent {
  form: FormGroup;

  @Output()
  onSubmit = new EventEmitter<any>();

  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {
    this.form = this.fb.nonNullable.group<BackSeriesSearchForm>({
      q: '',
      lang: 'fr',
    });
  }

  ngOnInit(): void {
    this.route.queryParams.pipe(distinctUntilChanged()).subscribe((params) => {
      const { q, lang } = params;

      if (q || true) {
        this.form.patchValue({ q: q || '', lang: lang || 'fr' });
        this.onSubmit.emit(this.form.value);
      }
    });
  }

  submit(event: SubmitEvent | Event) {
    event.stopPropagation();
    event.preventDefault();

    this.updateRouteParameters(this.form.value);

    this.onSubmit.emit(this.form.value);
  }

  updateRouteParameters(queryParams: any) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,

      // queryParamsHandling: 'merge', // remove to replace all query params by provided
    });
  }
}
