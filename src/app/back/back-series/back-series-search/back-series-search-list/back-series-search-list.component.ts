import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {
  IInfiniteScrollEvent,
  InfiniteScrollModule,
} from 'ngx-infinite-scroll';
import { Observable } from 'rxjs';
import { PaginatedMeta } from '../../../../types/paginated-result.type';
import { SerFindManyItem } from '../../../../types/ser-find-many-item.type';

@Component({
  selector: 'app-back-series-search-list',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    InfiniteScrollModule,
    MatListModule,
    MatProgressSpinnerModule,
  ],
  templateUrl: './back-series-search-list.component.html',
  styleUrl: './back-series-search-list.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BackSeriesSearchListComponent {
  @Input()
  items$?: Observable<SerFindManyItem[]>;

  @Input()
  loading$: Observable<boolean> | null = null;

  @Input()
  meta: PaginatedMeta | null = null;

  @Output()
  onScroll$ = new EventEmitter<IInfiniteScrollEvent>();

  constructor(private readonly sanitized: DomSanitizer) {}

  onScroll(event: IInfiniteScrollEvent) {
    this.onScroll$.emit(event);
  }
}
