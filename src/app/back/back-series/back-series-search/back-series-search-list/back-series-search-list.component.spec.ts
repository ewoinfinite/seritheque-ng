import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackSeriesSearchListComponent } from './back-series-search-list.component';

describe('BackSeriesSearchListComponent', () => {
  let component: BackSeriesSearchListComponent;
  let fixture: ComponentFixture<BackSeriesSearchListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BackSeriesSearchListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BackSeriesSearchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
