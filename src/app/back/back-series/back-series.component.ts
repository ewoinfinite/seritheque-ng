import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-back-series',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './back-series.component.html',
  styleUrl: './back-series.component.scss',
})
export class BackSeriesComponent {}
