import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
  MatTreeModule,
} from '@angular/material/tree';
import { RouterModule } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppService } from '../../../app.service';
import { Category } from '../../../types/category.type';
import { FlatTreeControl } from '@angular/cdk/tree';
import { TitleLevelComponent } from '../../../widgets/title-level/title-level.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MyTreeNode, UtilsService } from '../../../utils/utils.service';
import { SimpleCategory } from '../../../types/simple-category.type';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { MatBadgeModule } from '@angular/material/badge';

export interface FlatTreeNode<T> {
  expandable: boolean;
  level: number;
  node: T;
}

@Component({
  selector: 'app-category-list',
  standalone: true,
  imports: [
    CommonModule,
    MatTreeModule,
    RouterModule,
    TitleLevelComponent,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    // MatBadgeModule,
  ],
  templateUrl: './category-list.component.html',
  styleUrl: './category-list.component.scss',
  encapsulation: ViewEncapsulation.None,
})
export class CategoryListComponent implements OnDestroy {
  subscription = new Subscription();

  private _transformer = (node: MyTreeNode<SimpleCategory>, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      level: level,
      node: node.data,
    };
  };

  treeControl = new FlatTreeControl<FlatTreeNode<SimpleCategory>>(
    (node) => node.level,
    (node) => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    (node) => node.level,
    (node) => node.expandable,
    (node) => node.children
  );

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(
    private readonly appService: AppService,
    private readonly utils: UtilsService
  ) {
    this.subscription.add(
      this.appService.simpleCategoryFindMany('fr').subscribe((categories) => {
        this.dataSource.data = this.utils.flatToMyTreeNodes(
          categories,
          (category) => category.name
        );

        console.log(this.dataSource.data);
      })
    );
  }

  hasChild = (_: number, node: FlatTreeNode<Category>) => node.expandable;

  titleLevel(l: number): number {
    return Math.min(l + 2, 4);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
