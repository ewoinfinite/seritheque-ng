import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  inject,
} from '@angular/core';
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
import { AsyncPipe } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import {
  Observable,
  Subscription,
  combineLatest,
  firstValueFrom,
  lastValueFrom,
} from 'rxjs';
import { first, map, shareReplay } from 'rxjs/operators';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import {
  AuthUserResponse,
  AuthService,
  AuthLogoutResponse,
} from '../../auth/auth.service';
import { MatMenuModule } from '@angular/material/menu';
import { AppService } from '../../app.service';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-front-nav',
  templateUrl: './front-nav.component.html',
  styleUrl: './front-nav.component.scss',
  standalone: true,
  imports: [
    RouterModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    AsyncPipe,
    MatMenuModule,
    TranslateModule,
  ],
  // encapsulation: ViewEncapsulation.None,
})
export class FrontNavComponent implements OnInit, OnDestroy {
  private readonly breakpointObserver = inject(BreakpointObserver);

  private readonly subscription = new Subscription();

  isHandset$: Observable<BreakpointState> = this.breakpointObserver.observe([
    // Breakpoints.Large,
    // Breakpoints.Medium,
    // Breakpoints.Small,
    // Breakpoints.XSmall,
    '(max-width: 650px)',
  ]);
  // .pipe(
  //   map((result) => result.matches),
  //   shareReplay()
  // );

  isAuthenticated$: Observable<boolean>;
  user$: Observable<AuthUserResponse | null>;

  @ViewChild('drawer') drawer: any;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly service: AppService,
    private readonly translate: TranslateService,
    private readonly route: ActivatedRoute
  ) {
    this.isAuthenticated$ = this.authService.isAuthenticated$;
    this.user$ = this.authService.user$;

    this.translate.addLangs(['fr', 'en', 'es', 'it', 'pt', 'de']);
    this.translate.setDefaultLang('en');

    this.translate.use(this.getUserLanguage() || 'en');
  }

  ngOnInit(): void {
    // this.subscription.add()
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   *
   * @param redirectTo (Optional - default '/login')
   * @returns {AuthLogoutResponse | null} null means the user has canceled logout (CanDeactivateGuard)
   */
  async logout(redirectTo = '/logout'): Promise<AuthLogoutResponse | null> {
    // reflects CanDeactivateGuard - returns null if navigating on same url
    const canDeactivate = await (<Promise<boolean | null>>(
      this.router.navigateByUrl(redirectTo)
    ));

    // User canceled logout
    if (canDeactivate === false) return null;

    // if canDeactivate is null (redirectTo == currentUrl) or true
    return this.authService.logout();
  }

  /**
   * Close the drawer for mobile
   */
  async closeDrawer() {
    const isHandset = await firstValueFrom(this.isHandset$);

    if (isHandset.matches) this.drawer.close();
  }

  async gotoLastSearch(event: MouseEvent) {
    const lastSearch = this.service.serSearchHistoryGetLast();
    let url: string = '/series';
    let queryParams = undefined;

    if (lastSearch) {
      event.preventDefault();

      queryParams = lastSearch;

      this.router.navigate([url], {
        queryParams,
      });
    }
  }

  getUserLanguage(): string | undefined {
    return this.translate.getBrowserLang() || 'en';
  }

  setUserLanguage(lang: string): void {}

  useLanguage(language: string): void {
    this.translate.use(language);
  }

  currentLang(): string {
    return this.translate.currentLang;
  }
}
