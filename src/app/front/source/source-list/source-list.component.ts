import { Component } from '@angular/core';
import { AppService } from '../../../app.service';
import { Observable } from 'rxjs';
import { SimpleSource } from '../../../types/simple-source.type';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MarkedComponent } from '../../../widgets/marked/marked.component';

@Component({
  selector: 'app-source-list',
  standalone: true,
  imports: [CommonModule, MatCardModule, MarkedComponent],
  templateUrl: './source-list.component.html',
  styleUrl: './source-list.component.scss',
})
export class SourceListComponent {
  sources$: Observable<SimpleSource[]>;

  constructor(private readonly appService: AppService) {
    this.sources$ = this.appService.simpleSourceFindMany();
  }
}
