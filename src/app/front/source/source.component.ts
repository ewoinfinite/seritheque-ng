import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-source',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './source.component.html',
  styleUrl: './source.component.scss',
})
export class SourceComponent {}
