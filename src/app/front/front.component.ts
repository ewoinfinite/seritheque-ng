import { Component } from '@angular/core';
import { FrontNavComponent } from './front-nav/front-nav.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-front',
  standalone: true,
  imports: [RouterModule, FrontNavComponent],
  templateUrl: './front.component.html',
  styleUrl: './front.component.scss',
})
export class FrontComponent {}
