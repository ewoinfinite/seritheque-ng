import { Component, OnDestroy } from '@angular/core';
import { AppService } from '../../app.service';
import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
  combineLatest,
  first,
  map,
  mergeMap,
  startWith,
  switchMap,
  tap,
} from 'rxjs';
import { CommonModule } from '@angular/common';
import { Bookmark } from '../../types/bookmark.type';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-bookmark',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    RouterModule,
    MatIconModule,
    TranslateModule,
  ],
  templateUrl: './bookmark.component.html',
  styleUrl: './bookmark.component.scss',
})
export class BookmarkComponent implements OnDestroy {
  private readonly subscription = new Subscription();
  bookmarks$: Observable<any>;

  refresh$ = new Subject();

  constructor(private readonly appService: AppService) {
    this.bookmarks$ = this.refresh$.pipe(
      startWith(null),
      mergeMap(() => this.appService.bookmarkFindMany())
    );
  }

  createBookmark(label: string) {
    this.subscription.add(
      this.appService.bookmarkCreate(label).subscribe({
        complete: () => this.refresh$.next(null),
        error: (error) => {
          alert(error.error?.message || error.message);
        },
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
