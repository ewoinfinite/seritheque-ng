import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  delay,
  map,
  startWith,
  switchMap,
  tap,
} from 'rxjs';
import { filterIsDefined } from '../../../utils/rxjs-filter-is-defined.pipe';
import { MatCardModule } from '@angular/material/card';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { UtilsService } from '../../../utils/utils.service';
import { Title } from '@angular/platform-browser';
import { SerDetail } from '../../../types/ser-detail.type';
import { AppService } from '../../../app.service';
import { SimpleSerDetail } from '../../../types/ser-detail-simple.type';
import { MarkedComponent } from '../../../widgets/marked/marked.component';
import { MatIcon, MatIconModule } from '@angular/material/icon';
import { SerBookmarkComponent } from '../ser-bookmark/ser-bookmark.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-series-ser-detail',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatCardModule,
    MarkedComponent,
    MatIconModule,
    SerBookmarkComponent,
    MatProgressSpinnerModule,
    TranslateModule,
  ],
  providers: [UtilsService],
  templateUrl: './series-ser-detail.component.html',
  styleUrl: './series-ser-detail.component.scss',

  preserveWhitespaces: true,
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SeriesSerDetailComponent implements OnInit {
  private readonly _title = `Série Numérique`;
  title = this._title;

  private _nameNoSpace$ = new BehaviorSubject<string | null>(null);
  @Input()
  set nameNoSpace(nameNoSpace: string) {
    this._nameNoSpace$.next(nameNoSpace);
  }

  // ser?: SerDetail;

  sers$!: Observable<SimpleSerDetail[] | undefined>;

  readonly loading$ = new BehaviorSubject<boolean>(false);

  constructor(
    private readonly service: AppService,
    private readonly utils: UtilsService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly titleService: Title,
    private readonly translate: TranslateService
  ) {}

  ngOnInit() {
    // this.router.events
    //   .pipe(filter((event) => event instanceof NavigationEnd))
    //   .subscribe(() => {
    //     this.titleService.setTitle(this.title);
    //   });

    this.sers$ = this._nameNoSpace$.pipe(
      filterIsDefined(),
      tap(() => {
        this.loading$.next(true);
      }),
      switchMap((id) =>
        this.service.serFindByNameSimple(id).pipe(startWith(undefined))
      ),
      tap((sers) => {
        if (sers && sers.length) {
          this.title = `${this._title} - ${sers[0]?.name} - ${sers
            .map((ser) => ser.contents[0]?.title)
            .join(' / ')}`;
        } else this.title = `Loading`;

        this.titleService.setTitle(this.title);
      }),
      map((sers) => {
        if (sers && sers.length) {
          sers.forEach((ser) => {
            if (Array.isArray(ser.edges)) {
              ser.edges?.sort((a, b) => {
                return this.utils.smartSort(
                  { text: a.to.contents[0]?.title, order: a.order },
                  { text: b.to.contents[0]?.title, order: b.order }
                );
              });
            }
          });
        }

        return sers;
      }),
      tap((sers) => {
        if (sers) this.loading$.next(false);
      })
    );
  }

  md2html = this.utils.md2html;
}
