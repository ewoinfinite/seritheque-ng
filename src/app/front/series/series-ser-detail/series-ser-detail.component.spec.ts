import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesSerDetailComponent } from './series-ser-detail.component';

describe('SeriesSerDetailComponent', () => {
  let component: SeriesSerDetailComponent;
  let fixture: ComponentFixture<SeriesSerDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SeriesSerDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SeriesSerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
