import { Component, OnDestroy } from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  Subscription,
  catchError,
  combineLatest,
  debounceTime,
  distinctUntilChanged,
  map,
  mergeMap,
  of,
  scan,
  tap,
} from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { SeriesSearchResultComponent } from './series-search-result/series-search-result.component';
import {
  SeriesSearchForm,
  SeriesSearchFormComponent,
} from './series-search-form/series-search-form.component';
import { CommonModule } from '@angular/common';
import { IInfiniteScrollEvent } from 'ngx-infinite-scroll';
import { filterIsDefined } from '../../../utils/rxjs-filter-is-defined.pipe';
import {
  PaginatedResult,
  PaginatedMeta,
} from '../../../types/paginated-result.type';
import { AppService } from '../../../app.service';
import { SerFindManyItem } from '../../../types/ser-find-many-item.type';
import { pickBy } from 'lodash-es';
import { TranslateModule } from '@ngx-translate/core';

export type ErrorMessage = {
  title: string;
  message: string;
  description: string;
};

@Component({
  selector: 'app-series',
  standalone: true,
  imports: [
    CommonModule,
    SeriesSearchFormComponent,
    SeriesSearchResultComponent,
    MatCardModule,
    TranslateModule,
  ],
  templateUrl: './series-search.component.html',
  styleUrl: './series-search.component.scss',
})
export class SeriesSearchComponent implements OnDestroy {
  subscription = new Subscription();

  search$ = new BehaviorSubject<SeriesSearchForm | null>(null);

  page$ = new BehaviorSubject<number>(1);

  loading$ = new BehaviorSubject<boolean>(false);
  request$: Observable<PaginatedResult<SerFindManyItem> | null>;
  data$: Observable<SerFindManyItem[]>;
  meta: PaginatedMeta | null = null;

  scrollLimit = 4;

  errorMessages: ErrorMessage[] = [];

  debug: any;

  constructor(private readonly service: AppService) {
    this.request$ = combineLatest([
      this.search$.pipe(
        filterIsDefined(),
        map((search) => {
          // console.log(search);
          return {
            q: search.q?.trim() ?? '',
            // langId: search.langId,
            titleOnly: search.titleOnly,
            sourceIds: search.sourceIds ?? undefined,
          };
        }),
        // filter((search) => !!search?.q),
        tap((search) => {
          this.service.serSearchHistoryPush(search);
        }),
        tap(() => {
          this.page$.next(1);
          this.meta = null;
        })
      ),
      this.page$.pipe(distinctUntilChanged()),
    ]).pipe(
      debounceTime(0),
      // filter(([search]) => !!search?.q), // filter out empty search
      tap(() => this.loading$.next(true)),
      mergeMap(([search, page]) => {
        const searchCleaned = pickBy(search); // remove keys with nullish values

        return this.service.serSearch({ ...searchCleaned, page }).pipe(
          catchError((error, caught) => {
            // this.loading$.next(false);

            // this.debug = error;

            switch (error.status) {
              case 403:
                this.errorMessages = [
                  {
                    title: `Votre abonnement a expiré`,
                    message: error.message,
                    description: `
<p>
  Veuillez consulter <a href="https://www.grabovoi-numbers.world/my-account">votre compte</a> pour le renouveler.
</p>
<p>
  À bientôt sur Grabovoï Numbers.
</p>`,
                  },
                ];
                break;

              default:
                this.errorMessages = [
                  {
                    title: `Une erreur s'est produite`,
                    message: error.message,
                    description: `Merci de vérifier votre connection à internet et de recharger la page`,
                  },
                ];
            }

            // return throwError(() => new Error(error));

            return of(null);
          })
        );
      }),
      tap((response) => {
        if (response) this.meta = response.meta;
      }),
      tap(() => this.loading$.next(false))
    );

    this.data$ = this.request$.pipe(
      // accumulate data for infinityScroll
      scan((acc, val) => {
        if (!val) return acc;

        if (val.meta?.prev) {
          return acc.concat(val.data);
        }
        // if this is first page then it's a new search, reset data
        return val.data;
      }, <SerFindManyItem[]>[])
    );
  }

  search(form: any) {
    this.search$.next(form);
  }

  loadMore() {
    if (this.meta?.next && this.meta.currentPage === this.page$.value) {
      this.page$.next(this.meta.next);
    }
  }

  onScroll(event?: IInfiniteScrollEvent) {
    if (this.meta?.currentPage && this.meta?.currentPage < this.scrollLimit) {
      this.loadMore();
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

// switchOrMerge((searchPage: any) => searchPage),
// () => {
//   const a = <
//     T = {
//       search: SerSearchForm | null;
//       page: number;
//     }
//   >(
//     source: Observable<T>
//   ): Observable<PaginatedResult<SerFindManyItem>> => {
//     return source.pipe(
//       mergeMap((value, index) => {
//         const { search, page } = value;
//         return this.service.search({ ...search, page }).pipe(delay(500));
//       })
//     );
//   };
//   return a;
// },
// (): OperatorFunction<
//   {
//     search: SerSearchForm | null;
//     page: number;
//   },
//   PaginatedResult<SerFindManyItem>
// > => {
//   return mergeMap((value) => {
//     const { search, page } = value;
//     return this.service.search({ ...search, page }).pipe(delay(500));
//   });
// },
