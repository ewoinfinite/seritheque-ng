import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesSearchResultComponent } from './series-search-result.component';

describe('SeriesSearchResultComponent', () => {
  let component: SeriesSearchResultComponent;
  let fixture: ComponentFixture<SeriesSearchResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SeriesSearchResultComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SeriesSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
