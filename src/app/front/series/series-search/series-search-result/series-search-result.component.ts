import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {
  IInfiniteScrollEvent,
  InfiniteScrollDirective,
} from 'ngx-infinite-scroll';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { PaginatedMeta } from '../../../../types/paginated-result.type';
import { UtilsService } from '../../../../utils/utils.service';
import { SerFindManyItem } from '../../../../types/ser-find-many-item.type';

@Component({
  selector: 'app-series-search-result',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    InfiniteScrollDirective,
    MatListModule,
    MatProgressSpinnerModule,
  ],
  templateUrl: './series-search-result.component.html',
  styleUrl: './series-search-result.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SeriesSearchResultComponent {
  @Input()
  items$?: Observable<SerFindManyItem[]>;

  @Input()
  loading$: Observable<boolean> | null = null;

  @Input()
  meta: PaginatedMeta | null = null;

  @Output()
  onScroll$ = new EventEmitter<IInfiniteScrollEvent>();

  constructor(
    private readonly sanitized: DomSanitizer,
    private readonly utils: UtilsService
  ) {}

  onScroll(event: IInfiniteScrollEvent) {
    this.onScroll$.emit(event);
  }

  // md2txt(md: string) {
  //   return this.utils.md2txt(md);
  // }
}
