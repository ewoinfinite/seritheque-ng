import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesSearchFormComponent } from './series-search-form.component';

describe('SeriesSearchFormComponent', () => {
  let component: SeriesSearchFormComponent;
  let fixture: ComponentFixture<SeriesSearchFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SeriesSearchFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SeriesSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
