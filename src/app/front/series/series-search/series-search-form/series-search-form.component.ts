import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';

import { MatInputModule } from '@angular/material/input';
import {
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatFormFieldModule,
} from '@angular/material/form-field';
import { MatIcon } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {
  Observable,
  Subscription,
  debounceTime,
  distinct,
  distinctUntilChanged,
  filter,
  first,
  tap,
} from 'rxjs';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CheckboxModule } from 'primeng/checkbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { Source, SourceWithContents } from '../../../../types/source.type';
import { AppService } from '../../../../app.service';
import { CommonModule } from '@angular/common';
import { SimpleSource } from '../../../../types/simple-source.type';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
// import { pickBy } from 'lodash-es';
import * as _ from 'lodash-es';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

export interface SeriesSearchForm {
  q: string;
  // langId: string;
  titleOnly: boolean;
  sources: Source[];
  sourceIds: number[];
}

export type ModelFormGroup<T> = FormGroup<{
  [K in keyof T]: FormControl<T[K] | null>;
}>;

@Component({
  selector: 'app-series-search-form',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIcon,
    MatButtonModule,
    CheckboxModule,
    MultiSelectModule,
    MatCheckboxModule,
    TranslateModule,
  ],
  templateUrl: './series-search-form.component.html',
  styleUrl: './series-search-form.component.scss',
  providers: [
    // sets default value for all mat-form-fields
    // aka <mat-form-field subscriptSizing="dynamic">
    // credit https://stackoverflow.com/questions/53684763/how-to-remove-space-bottom-mat-form-field
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        subscriptSizing: 'dynamic',
      },
    },
  ],
  preserveWhitespaces: true,
  encapsulation: ViewEncapsulation.None,
})
export class SeriesSearchFormComponent implements OnInit, OnDestroy {
  private readonly subscription = new Subscription();

  searchForm: ModelFormGroup<SeriesSearchForm>;

  @Output()
  onSubmit = new EventEmitter<any>();

  sources$: Observable<SimpleSource[]> = this.service.simpleSourceFindMany();

  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly service: AppService,
    private readonly translate: TranslateService
  ) {
    this.searchForm = this.fb.nonNullable.group({
      q: new FormControl<string>(''),
      // langId: new FormControl<string>('fr'),
      titleOnly: new FormControl<boolean>(false),
      sources: new FormControl<Source[]>([]),
      sourceIds: new FormControl<number[]>([]),
    });
  }

  ngOnInit(): void {
    this.loadFormValues();

    this.subscription.add(
      this.searchForm.valueChanges
        .pipe(
          debounceTime(400),
          distinctUntilChanged((a, b) => _.isEqual(a, b)),
          // distinctUntilChanged((a, b) => a.q?.trim() === b.q?.trim()),
          // tap(console.log),

          // only trigger search if q.length
          // filter((value) => {
          //   const q = value.q?.trim() ?? '';
          //   return q.length > 2;
          // }),
          tap((_value) => this.submit())
        )
        .subscribe()
    );

    this.subscription.add(
      this.translate.onLangChange.subscribe(() => {
        // this.updateRouteParameters(_.pickBy(this.searchForm.value));
        this.submit();
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  loadFormValues() {
    this.subscription.add(
      this.route.queryParams.pipe(first()).subscribe((params) => {
        const { q, titleOnly } = params;

        const sourceIdsRaw = params['sourceIds'];

        const sourceIds = (
          Array.isArray(sourceIdsRaw)
            ? sourceIdsRaw
            : [sourceIdsRaw ?? undefined]
        )
          .filter((v) => Number(v))
          .map((v) => Number(v));

        // console.log(params);

        // trigger search if q
        if (q || true) {
          this.searchForm.patchValue({
            q,
            // langId: langId || 'fr',
            titleOnly,
            sourceIds,
          });

          this.onSubmit.emit(this.searchForm.value);
        }
      })
    );
  }

  submit(event?: SubmitEvent | Event) {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }

    const formValue = _.pickBy(this.searchForm.value);

    // formValue.sourceIds = formValue.sources?.map((s) => s.id);

    this.updateRouteParameters(formValue);
    this.onSubmit.emit(formValue);
  }

  updateRouteParameters(formValue: Partial<SeriesSearchForm>) {
    const queryParams = {
      q: formValue.q || undefined,
      // langId: this.translate.currentLang || undefined,
      titleOnly: formValue.titleOnly ? 1 : undefined,
      sourceIds: formValue.sourceIds || undefined,
    };

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,

      // queryParamsHandling: 'merge', // remove to replace all query params by provided
    });
  }

  clearSelectedSources(e: MouseEvent) {
    e.stopPropagation();
    e.preventDefault();

    this.searchForm.controls['sourceIds'].setValue([]);
  }
}
