import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SerBookmarkComponent } from './ser-bookmark.component';

describe('SerBookmarkComponent', () => {
  let component: SerBookmarkComponent;
  let fixture: ComponentFixture<SerBookmarkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SerBookmarkComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SerBookmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
