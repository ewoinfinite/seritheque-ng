import { CommonModule } from '@angular/common';
import { Component, Input, OnDestroy } from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
  first,
  map,
  mergeMap,
  shareReplay,
  startWith,
  switchMap,
  tap,
} from 'rxjs';
import { AppService } from '../../../app.service';
import { Ser } from '../../../types/ser.type';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { filterIsDefined } from '../../../utils/rxjs-filter-is-defined.pipe';

@Component({
  selector: 'app-ser-bookmark',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIconModule],
  templateUrl: './ser-bookmark.component.html',
  styleUrl: './ser-bookmark.component.scss',
})
export class SerBookmarkComponent implements OnDestroy {
  private readonly subscription = new Subscription();

  @Input()
  serId!: number;

  isBookmarked$: Observable<boolean>;

  bookmarks$: Observable<any[]>;

  refresh$ = new Subject();

  loading$ = new BehaviorSubject<boolean>(false);

  constructor(private readonly appService: AppService) {
    // this.isBookmarked$ = new Observable().pipe(
    //   startWith(false),
    //   switchMap((_) => this.appService.bookmarkFindMany()),
    //   map((bookmarks) => {
    //     return bookmarks.find((bookmark) =>
    //       bookmark.bookmarkToSers.find(
    //         (bookmarkToSer: any) => bookmarkToSer.serId === this.ser.id
    //       )
    //     )
    //       ? true
    //       : false;
    //   })
    // );

    this.bookmarks$ = this.refresh$.pipe(
      // shareReplay(1),
      startWith(null),
      tap(() => this.loading$.next(true)),
      switchMap((_) => this.appService.bookmarkFindMany()),
      map((bookmarks) => {
        // return (
        //   bookmarks.find((bookmark) =>
        //     bookmark.bookmarkToSers.find(
        //       (bookmarkToSer: any) => bookmarkToSer.serId === this.ser.id
        //     )
        //   ) || false
        // );

        return bookmarks.filter((bookmark) =>
          bookmark.bookmarkToSers.some(
            (bookmarkToSer: any) => bookmarkToSer.serId === this.serId
          )
        );
      }),
      tap(() => this.loading$.next(false))
    );

    this.isBookmarked$ = this.bookmarks$.pipe(
      // map((bookmarks) =>
      //   bookmarks === null ? null : Boolean(bookmarks?.length)
      // ),
      map((bookmarks) => Boolean(bookmarks?.length))
      // startWith(null),
      // tap(console.log)
    );
  }

  addBookmark$(serId: number, label: string = 'default') {
    return this.appService.bookmarkAddSer(serId, label).pipe(
      startWith(null),
      tap(() => this.loading$.next(true)),
      filterIsDefined(),
      tap(() => this.loading$.next(false)),
      tap((_) => this.refresh$.next(null))
      // first()
    );
  }

  delBookmark$(serId: number, label: string = 'default') {
    return this.appService.bookmarkDelSer(serId, label).pipe(
      startWith(null),
      tap(() => this.loading$.next(true)),
      filterIsDefined(),
      tap(() => this.loading$.next(false)),
      tap((_) => this.refresh$.next(null))
      // first()
    );
  }

  toggleBookmark(serId: number, label: string = 'default') {
    this.subscription.add(
      this.bookmarks$
        .pipe(
          mergeMap((bookmarks) =>
            bookmarks?.length
              ? this.delBookmark$(serId, bookmarks[0].label)
              : this.addBookmark$(serId, label)
          ),
          first()
        )
        .subscribe()
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
