import { Injectable, inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  private readonly toastr = inject(ToastrService);

  // toasts: { message: string; duration: number; type: 'success' | 'error' }[] =
  //   [];

  error(message: string, title: string, duration: number = 10000) {
    // this.toasts.push({ message, duration, type });
    // setTimeout(() => this.remove(0), duration);

    this.toastr.error(message, title, { timeOut: duration });
  }

  success(message: string, title: string, duration: number = 10000) {
    // this.toasts.push({ message, duration, type });
    // setTimeout(() => this.remove(0), duration);

    this.toastr.success(message, title, { timeOut: duration });
  }

  warning(message: string, title: string, duration: number = 10000) {
    // this.toasts.push({ message, duration, type });
    // setTimeout(() => this.remove(0), duration);

    this.toastr.warning(message, title, { timeOut: duration });
  }

  // remove(index: number) {
  //   // this.toasts.splice(index, 1);
  // }
}
