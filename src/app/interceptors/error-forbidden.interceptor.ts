import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, throwError } from 'rxjs';
import { ToastService } from '../toast.service';

export const errorForbiddenInterceptor: HttpInterceptorFn = (req, next) => {
  const router = inject(Router);
  const toast = inject(ToastService);

  const result = next(req).pipe(
    catchError((error) => {
      if (error instanceof HttpErrorResponse) {
        // Handle HTTP errors

        switch (error.status) {
          case 403:
            // router.navigateByUrl('/error/forbidden');
            toast.error(
              `Veuillez consulter <a href="https://www.grabovoi-numbers.world/my-account">votre compte</a> pour le renouveler.`,
              `Votre Abonnement a expiré`
            );
            break;

          default:
        }
      } else {
        // Handle non-HTTP errors
        // error.error instanceof ErrorEvent
      }

      return throwError(() => error);
    })
  );

  return result;
};
