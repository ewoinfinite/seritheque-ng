import { Injectable } from '@angular/core';
import { MarkedOptions, marked } from 'marked';
import { TreeNode } from 'primeng/api';
import { isDefined } from './filter-is-defined';
// import { convert } from 'html-to-text';

export interface RomanSortOptions {
  prefix: string;
  suffix: string;
}

export interface IGetLabelFn<T> {
  (arg0: T): string;
}

export type MyTreeNode<T> = {
  data: T;
  children: MyTreeNode<T>[];
};

export type ArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  readonly md2html = (
    text: string,
    options?: Omit<MarkedOptions, 'async'>
  ): string => {
    // linter seems tu be confused: string | Promise<string>

    if (!text) return '';

    const html = <string>marked.parse(text, {
      silent: false,
      gfm: true,
      breaks: true,
      ...options,
      async: false,
    });

    return this.fixHtml(html);
  };

  // readonly md2txt = (md: string): string => {
  //   return convert(this.md2html(md));
  // };

  readonly txtLocal = (txt: string, lang?: string | null): string => {
    switch (lang) {
      // @ts-expect-error: Case with fallthrough in instruction switch. ts(7029)
      case 'fr':
        txt = txt.replace(/(\S) ([?!:;])(\s|$)/gmu, '$1\u202F$2$3'); // \u202F = Espace insécable étroite (NNBSP)

      case 'default':
      default:
    }

    return txt;
  };

  sortByNumberInString(a: string, b: string): number {
    if (a === b) return 0;

    const aNumString = /\d+/.exec(a);
    const bNumString = /\d+/.exec(b);

    const aNum = aNumString && Number(aNumString[0]);
    const bNum = bNumString && Number(bNumString[0]);

    if (aNum || bNum) {
      if (aNum !== bNum) {
        if (aNum === null) return -1;
        if (bNum === null) return 1;

        if (aNum > bNum) return 1;
        return -1;
      }
    }

    return 0;
  }

  sorByRomanNumberInString(
    a: string,
    b: string,
    options?: RomanSortOptions
  ): number {
    options = options || { prefix: '(', suffix: ')' };

    const romanNum = [
      null,
      'I',
      'II',
      'III',
      'IV',
      'V',
      'VI',
      'VII',
      'VIII',
      'IX',
      'X',
      'XI',
      'XII',
    ];

    const aRoman = romanNum.indexOf(
      romanNum.find((r) =>
        a.includes(`${options.prefix}${r}${options.suffix}`)
      ) || null
    );

    const bRoman = romanNum.indexOf(
      romanNum.find((r) =>
        b.includes(`${options.prefix}${r}${options.suffix}`)
      ) || null
    );

    if (aRoman || bRoman) {
      if (aRoman !== bRoman) {
        if (aRoman === null) return -1;
        if (bRoman === null) return 1;

        if (aRoman > bRoman) return 1;
        return -1;
      }
    }

    return 0;
  }

  /**
   * on order === null it will try to sort based first on finding a number in text or a roman number in brackets ex: (I) (II)
   * @param a
   * @param b
   * @returns sort number
   */

  smartSort(
    a: { text: string; order?: number | null },
    b: { text: string; order?: number | null },
    options?: { romanSortOptions?: RomanSortOptions }
  ): number {
    a.order = a.order || null;
    b.order = b.order || null;

    if ((a.order === null && b.order === null) || a.order === b.order) {
      if (a.text === b.text) return 0;

      const sortByNum = this.sortByNumberInString(a.text, b.text);
      if (sortByNum) return sortByNum;

      const sortByRoman = this.sorByRomanNumberInString(
        a.text,
        b.text,
        options?.romanSortOptions
      );
      if (sortByRoman) return sortByRoman;

      // finally we sort by alphabetic order
      if (a.text > b.text) return 1;
      return -1;
    }

    // TODO: check this means nulls last
    if (a.order === null) return -1;
    if (b.order === null) return 1;

    if (a.order > b.order) return 1;
    return -1;
  }

  fixHtml(html: string): string {
    const parser = new DOMParser();
    const doc = parser.parseFromString(html, 'text/html');
    return doc.documentElement.getElementsByTagName('body')[0].innerHTML || '';
  }

  flatToPrimeTreeNodes<
    T extends {
      id: number | string;
      parentId: T['id'] | null;
      order: number | null;
    }
  >(
    nodes: T[],
    getLabelFn: IGetLabelFn<T> = (node): string => String(node.id),
    notSelectableNodes: T['id'][] = [],
    parentNode?: T
  ): TreeNode<T>[] {
    const sort = (a: T, b: T) => {
      if (a.order !== b.order) {
        if (a.order === null) return -1;
        if (b.order === null) return 1;
        if (a.order > b.order) return 1;
        return -1;
      }

      // so a.order === null and b.order === null

      const aLabel = getLabelFn(a);
      const bLabel = getLabelFn(b);
      if (aLabel > bLabel) return 1;
      if (aLabel < bLabel) return -1;

      if (a.id > b.id) return 1;
      return -1;
    };

    const childNodes = (
      nodes: T[],
      parentNode: T | undefined | null = null
    ): TreeNode<T>[] => {
      return nodes
        .filter((node) => node.parentId === (parentNode?.id ?? null))
        .sort(sort)
        .map((node) => {
          const result: TreeNode<T> = {
            key: String(node.id),
            data: node,
            label: getLabelFn(node),
            children: childNodes(nodes, node),
            selectable: !notSelectableNodes.includes(node.id),
          };

          return result;
        })
        .filter((node) => isDefined(node.data));
    };

    return childNodes(nodes, parentNode);
  }

  readonly flatToPrimeTreeNode = <
    T extends {
      id: number | string;
      parentId: T['id'] | null;
      order: number | null;
    }
  >(
    rootNode: T,
    ...args: Parameters<typeof this.flatToPrimeTreeNodes<T>>
  ) => {
    args[3] = rootNode;

    const result: TreeNode<T> | undefined = this.flatToPrimeTreeNodes(
      ...args
    ).find((node) => node.data?.id === rootNode.id);

    if (!isDefined(result))
      throw new Error(`Could not find node with id:${rootNode.id}`);

    return result;
  };

  readonly flatToMyTreeNodes = <
    T extends {
      id: number | string;
      parentId: T['id'] | null;
      order: number | null;
    }
  >(
    nodes: T[],
    getLabelFn: IGetLabelFn<T> = (node): string => String(node.id),
    parentNode?: T
  ): MyTreeNode<T>[] => {
    const sort = (a: T, b: T) => {
      if (a.order !== b.order) {
        if (a.order === null) return -1;
        if (b.order === null) return 1;
        if (a.order > b.order) return 1;
        return -1;
      }

      // so a.order === null and b.order === null

      const aLabel = getLabelFn(a);
      const bLabel = getLabelFn(b);
      if (aLabel > bLabel) return 1;
      if (aLabel < bLabel) return -1;

      if (a.id > b.id) return 1;
      return -1;
    };

    const childNodes = (
      nodes: T[],
      parentNode: T | undefined | null = null
    ): MyTreeNode<T>[] => {
      return nodes
        .filter((node) => node.parentId === (parentNode?.id ?? null))
        .sort(sort)
        .map((node) => {
          const result: MyTreeNode<T> = {
            data: node,
            children: childNodes(nodes, node),
          };

          return result;
        })
        .filter((node) => isDefined(node.data));
    };

    return childNodes(nodes, parentNode);
  };

  // return of(this.sanitized.bypassSecurityTrustHtml(html));
}
