export function filterIsNot<T, G extends unknown[]>(...forbiddenValues: G) {
  return (value: T): value is Exclude<T, G[number]> => {
    for (const forbiddenValue of forbiddenValues) {
      if (value === forbiddenValue) {
        return false;
      }
    }

    return true;
  };
}
