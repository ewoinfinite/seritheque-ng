import { OperatorFunction, filter } from 'rxjs';

// credit https://codegen.studio/blog/uk4myc/how-to-create-custom-pipe-operators-in-rxjs/
export function filterIsDefined<T>(): OperatorFunction<
  T,
  Exclude<T, null | undefined>
> {
  return filter(
    (value): value is Exclude<T, null | undefined> =>
      value !== null && value !== undefined
  );
}
