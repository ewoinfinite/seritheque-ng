import { OperatorFunction, filter } from 'rxjs';

// credit https://codegen.studio/blog/uk4myc/how-to-create-custom-pipe-operators-in-rxjs/
export function filterIsNot<T, G extends unknown[]>(
  ...forbiddenValues: G
): OperatorFunction<T, Exclude<T, G[number]>> {
  return filter((value): value is Exclude<T, G[number]> => {
    for (const forbiddenValue of forbiddenValues) {
      if (value === forbiddenValue) {
        return false;
      }
    }

    return true;
  });
}
