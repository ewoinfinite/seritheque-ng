export const environment = {
  production: false,

  apiBase: 'http://localhost:3000',

  oauthEndpoint: '/auth/wp-oauth',

  // when logout from app logout from WordPress too
  // logoutUrl:
  //   'https://www2.grigori-grabovoi.academy/logout?redirect=https://www.grabovoi-numbers.world/',
  logoutUrl: 'https://www.grabovoi-numbers.world/logout',
};
